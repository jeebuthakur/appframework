package practise.AppiumFramework;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.springframework.aop.scope.ScopedObject;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class APIDemoApp extends DefineCapabilities{

	@Test
	public void apiDemo() throws IOException
	
	{
		
		Service= startServer();
		
		AndroidDriver<AndroidElement>  driver=Capabilities("apiDemo");	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		ApiDemoFormPage api=new ApiDemoFormPage(driver);
		
		//driver.findElementByXPath("//android.widget.Button[@text='CONTINUE']").click();
		System.out.println("Click on Continue");
		api.textContinue.click();
		
		//driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
		System.out.println("Click on OK");
		api.textOk.click();
		
		//driver.findElementByXPath("//android.widget.TextView[@text='Preference']").click();
		System.out.println("Click on OK");
		api.textPreference.click();
		
		//driver.findElementByXPath("//android.widget.TextView[@text='3. Preference dependencies']").click();
		System.out.println("Click on 3. Preference dependencies");
		api.textDependencies.click();
		
		//driver.findElementByXPath("//android.widget.CheckBox[@index='0']").click();
		System.out.println("Click Index Zero");
		api.textZero.click();
		
        //driver.findElementByXPath("//android.widget.TextView[@text='WiFi settings']").click();
		System.out.println("Select WiFi");
        api.WiFi.click();
        
        //driver.findElementByXPath("//android.widget.EditText").sendKeys("Hello");
        System.out.println("Select Edit Text and type hello");
        api.EditText.sendKeys("Hello");
        
       // driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
        System.out.println("Click on Ok");
        api.EditOK.click();
        Service.stop();
	}
	}















