package practise.AppiumFramework;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;



public class EcommerceApp extends DefineCapabilities{

	
	public void ecom() throws IOException, InterruptedException
	{
		Service= startServer();
		AndroidDriver<AndroidElement>  driver=Capabilities("generalstore");	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		FirstPageEcom first=new FirstPageEcom(driver);
		first.getSelectCountry().click();
		Utilities u=new Utilities(driver);
		u.scrollToText("Brazil");
		first.getSelectCountry().click();
		first.getNamePerson().sendKeys("Rajeev");
		driver.hideKeyboard();
		first.getSelectRadio().click();
		first.clickShop.click();
		
	    //Second Page Start.
	   /* driver.findElementsByXPath("//*[@text='ADD TO CART']").get(0).click();
		driver.findElementsByXPath("//*[@text='ADD TO CART']").get(0).click();
		driver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
		Thread.sleep(4000);
		
		//Third Page Start.
		String amount1= driver.findElementsById("com.androidsample.generalstore:id/productPrice").get(0).getText();
		
		amount1 =amount1.substring(1);
		double amount1Value= Double.parseDouble(amount1);
	
		String amount2= driver.findElementsById("com.androidsample.generalstore:id/productPrice").get(1).getText();
		amount2 =amount2.substring(1);// Out Put Showing 120+120=240.
		double amount2Value= Double.parseDouble(amount2);
		double SumofTotalAmount=amount1Value+amount2Value;
		System.out.println(SumofTotalAmount+"Total Amount of the Products which we are add in Cart");
		
		String total= driver.findElementById("com.androidsample.generalstore:id/totalAmountLbl").getText();
		total=total.substring(1);
		double totalValue=Double.parseDouble(total);
		System.out.println(totalValue+"Total Value of the Products which we are add in Cart");
		
		driver.findElementByAndroidUIAutomator("text(\"Send me e-mails on discounts related to selected products in future\")").click();
		
		TouchAction t=new TouchAction(driver);
		WebElement pn= driver.findElementByXPath("//*[@text='Please read our terms of conditions']");
		t.longPress(longPressOptions().withElement(element(pn)).withDuration(ofSeconds(2))).release().perform();
		driver.findElementByXPath("//*[@text='CLOSE']").click();
		driver.findElementById("com.androidsample.generalstore:id/btnProceed").click(); */
		Service.stop();
		
}
	@BeforeTest
	public void killAllNodes() throws IOException, InterruptedException
	{
	//taskkill /F /IM node.exe
		Runtime.getRuntime().exec("taskkill /F /IM node.exe");
		Thread.sleep(3000);
	}
}
