package practise.AppiumFramework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FirstPageEcom 
{
public FirstPageEcom(AndroidDriver<AndroidElement>driver) 
{
	PageFactory.initElements(new AppiumFieldDecorator(driver),  this);	
}
@AndroidFindBy(id="com.androidsample.generalstore:id/spinnerCountry")
public WebElement spinnerCountry;

@AndroidFindBy(xpath="//*[@text='Brazil']")
public WebElement enterCountry;

//driver.findElementByAndroidUIAutomator("text(\"Enter name here\")").sendKeys("Rajeev");
@AndroidFindBy(uiAutomator="text(\\\"Enter name here\\\")")
public WebElement enterName;

//driver.findElementByAndroidUIAutomator("text(\"Female\")").click();
@AndroidFindBy(xpath="//*[@text='Female']")
public WebElement radiobutton;

//driver.findElementByAndroidUIAutomator("text(\"Let's  Shop\")").click();
@AndroidFindBy(uiAutomator="text(\\\"Let's  Shop\\\")")
public WebElement clickShop;

public WebElement getSelectCountry()
{
	System.out.println("Please Select Country");
	return spinnerCountry;
}
public WebElement getenterCountry()
{
	System.out.println("Please Enter Country");
	return enterCountry;
}
public WebElement getNamePerson()
{
	System.out.println("Please Enter Name");
	return enterName;
}
public WebElement getSelectRadio()
{
	System.out.println("Please Select Radio Button");
	return radiobutton;
}
}
