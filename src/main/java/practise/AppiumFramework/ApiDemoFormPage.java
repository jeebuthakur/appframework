package practise.AppiumFramework;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ApiDemoFormPage 
{
	public  ApiDemoFormPage(AndroidDriver<AndroidElement> driver)
	{
		PageFactory.initElements(new AppiumFieldDecorator(driver),  this);
	}
	//driver.findElementByXPath("//android.widget.Button[@text='CONTINUE']").click();
	@AndroidFindBy(xpath="//android.widget.Button[@text='CONTINUE']")
	public WebElement textContinue;
	
	//driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
	@AndroidFindBy(xpath="//android.widget.Button[@text='OK']")
	public WebElement textOk;
	
	//driver.findElementByXPath("//android.widget.TextView[@text='Preference']").click();
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Preference']")
	public WebElement textPreference;
	
	//driver.findElementByXPath("//android.widget.TextView[@text='3. Preference dependencies']").click();
	@AndroidFindBy(xpath="//android.widget.TextView[@text='3. Preference dependencies']")
	public WebElement textDependencies;
	
	//driver.findElementByXPath("//android.widget.CheckBox[@index='0']").click();
	@AndroidFindBy(xpath="//android.widget.CheckBox[@index='0']")
	public WebElement textZero;
	
    //driver.findElementByXPath("//android.widget.TextView[@text='WiFi settings']").click();
	@AndroidFindBy(xpath="//android.widget.TextView[@text='WiFi settings']")
	public WebElement WiFi;
	
    //driver.findElementByXPath("//android.widget.EditText").sendKeys("Hello");
	@AndroidFindBy(xpath="//android.widget.EditText")
	public WebElement EditText;
	
    //driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
	@AndroidFindBy(xpath="//android.widget.Button[@text='OK']")
	public WebElement EditOK;
	
}
