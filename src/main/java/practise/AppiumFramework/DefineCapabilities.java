package practise.AppiumFramework;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class DefineCapabilities 
{
	public static AppiumDriverLocalService Service; 
	public static AndroidDriver<AndroidElement>driver;
	
public AppiumDriverLocalService startServer()
{
	//To Start the Appium server Automatically.
	boolean flag= checkIfServerIsRunning(4723);
	if (!flag)
	{
	Service=AppiumDriverLocalService.buildDefaultService();
	Service.start();
	}
	return Service;
}
//Below Program for check Our Appium Server is Running or Not.
public static boolean checkIfServerIsRunning(int port) {
	boolean isServerRunning= false;
	ServerSocket serverSocket;
try {
	serverSocket= new ServerSocket(port);
	serverSocket.close();
}
catch(IOException e) {
	//If Control comes here,Then it means that the port is in use
	isServerRunning=true;
} finally {
	serverSocket=null; 
}
return isServerRunning;
}
	public static AndroidDriver<AndroidElement> Capabilities(String appname) throws MalformedURLException
	{
		
		DesiredCapabilities dc=new DesiredCapabilities();
		//startEmulator();
		dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "Thakur");
		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.0.0");
		//dc.setCapability("appPackage", "com.androidsample.generalstore");
		//dc.setCapability("appActivity", "com.androidsample.generalstore.SplashActivity");
		dc.setCapability("appPackage", "io.appium.android.apis");
		dc.setCapability("appActivity", "io.appium.android.apis.ApiDemos");
		
		
		AndroidDriver<AndroidElement>driver=new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),dc);
        return driver;
	}
//Get Screen Shot of Defects Screen.
	/*public static void getScreenShort(String s) throws IOException
	{
	File scrfile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(scrfile, new File("C:\\Users\\jeebu\\eclipse-workspace\\AppiumFramework\\src\\main\\java\\DefectSnap-Short")); */
	/*public void getScreenshot() throws IOException{
Date currentdate=new Date();
String screenshotfilename=currentdate.toString().replace("", "-").replace(":", "-");
File screenshotFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
FileUtils.copyFile(screenshotFile, new File(".//screenshot//"+ screenshotfilename+".png"));*/
	}

